
; this list holds all the variables
(defvar *vars* nil)
; this one holds all the labels
(defvar *labels* nil)
; the stack to push variables onto (for local variables)
(defvar *varstack* nil)
; and the block (function) return stack
(defvar *returnstack* nil)
; and this is the current line index
(defvar *currline* 0)

;; variable management
; variables are stored as ((name1 value1) (name2 value2) (name3 value3))
(defun get-var (name vars)
  (if (null vars) ; a empty list will not contain the given variable
      nil
      (if (string= (nth 0 (car vars)) name) ; did we find the variable?
	  (nth 1 (car vars)) ; return the associated value
	  (get-var name (cdr vars))))) ; try to find it in the rest of the list
(defun set-var (name value)
  (dotimes (i (length *vars*)) ; find the existing value?
    (if (string= (nth 0 (nth i *vars*)) name)
	(progn
	  (setf (nth 1 (nth i *vars*)) value)
	  (return-from set-var))))
  (push (list name value) *vars*)) ; add a new one

;; label management
; labels are stored just like variables
(defun get-label (name labels)
  (if (null labels) ; a empty list will not contain the given label
      nil
      (if (string= (nth 0 (car labels)) name) ; did we find the label?
	  (nth 1 (car labels)) ; return the associated line number
	  (get-var name (cdr labels))))) ; try to find it in the rest of the list
(defun set-label (name value)
  (dotimes (i (length *labels*)) ; find the existing line number?
    (if (string= (nth 0 (nth i *labels*)) name)
	(progn
	  (setf (nth 1 (nth i *labels*)) value)
	  (return-from set-label))))
  (push (list name value) *labels*)) ; add a new one


;; to check if we can convert strings to integers
(defun valid-int (str)
  (if (= (length str) 0)
      (return-from valid-int nil))
  (if (char= (aref str 0) #\-) ; negative integers can be written nicely ("-5" instead of "0-5")
      (setq str (subseq str 1 (length str))))
  (dotimes (i (length str))
    (let ((c (aref str i)))
      (if (not (or
	    (char= c #\0) (char= c #\1) (char= c #\2) (char= c #\3)
	    (char= c #\4) (char= c #\5) (char= c #\6)
	    (char= c #\7) (char= c #\8) (char= c #\9)
	    ))
	  (return-from valid-int nil))))
  t) ; if all the characters were numbers, the entire thing is too

;; removes all spaces and tabs from the string
(defun condense (str)
  (if (string= str "") ; base case
      ""
      (let ((c (aref str 0))) ; check the next character
	    (if (or (char= c #\Space) (char= c #\Tab))
	       (condense (subseq str 1 (length str))) ; remove the character from the final string
	       (concatenate 'string (string c) (condense (subseq str 1 (length str)))))))) ; keep the character
;; removes all spaces and tabs from the start of the string
(defun condense-start (str)
  (if (string= str "") ; base case
      ""
      (let ((c (aref str 0))) ; check the next character
	    (if (or (char= c #\Space) (char= c #\Tab))
	       (condense-start (subseq str 1 (length str))) ; remove the character from the final string
	       str)))) ; keep the rest of the string

;; checks if the string contains the given substring starting at n
(defun check-substring (str sstr n)
  (if (< (- (length str) n) (length sstr))
      nil
      (string= sstr (subseq str n (+ n (length sstr))))))
;; finds the position of the substring in the given string
(defun find-substring (str sstr)
  (dotimes (i (- (length str) (length sstr)))
    (if (check-substring str sstr i)
	(return-from find-substring i)))
  nil) ; string not found..

;; Main parsing function
; takes the string to be parsed, and the current variables
(defun interpret (str)
  ;(format t "(interpret '~a') ~a ~a" str *vars* *labels*) (terpri)
  (cond
    ((and (char= (aref str 0) #\") (char= (aref str (- (length str) 1)) #\")) ; a string
     (subseq str 1 (- (length str) 1))) ; return the contents of the string
    
    ;; I/O
    ((check-substring str "show " 0) ; format: "show {expression}"
     (format t "~a" (interpret (subseq str 5 (length str)))) (terpri)
     t)
    ((check-substring str "write " 0) ; format: "write {expression}" to show without a newline
     (format t "~a" (interpret (subseq str 6 (length str))))
     (finish-output) ; flush stdout to make sure that the string is displayed
     t)
    ((check-substring str "read into " 0) ; format: "read into {variable name}"
     (set-var (subseq str 10 (length str)) (read t))
     t)
    
    ;; boolean logic
    ((let ((n (find-substring str " is not "))) ; check to see if two things are unequal
       (if n
	   (return-from interpret (not (eql (interpret (subseq str 0 n)) (interpret (subseq str (+ n 8) (length str)))))))))
    ((let ((n (find-substring str " is less than or equal to "))) ; check to see if a is less than or equal to b
       (if n
	   (return-from interpret (<= (interpret (subseq str 0 n)) (interpret (subseq str (+ n 26) (length str))))))))
    ((let ((n (find-substring str " is more than or equal to "))) ; check to see if a is more than or equal to b
       (if n
	   (return-from interpret (>= (interpret (subseq str 0 n)) (interpret (subseq str (+ n 26) (length str))))))))
    ((let ((n (find-substring str " is less than "))) ; check to see if a is less than b
       (if n
	   (return-from interpret (< (interpret (subseq str 0 n)) (interpret (subseq str (+ n 14) (length str))))))))
    ((let ((n (find-substring str " is more than "))) ; check to see if a is more than b
       (if n
	   (return-from interpret (> (interpret (subseq str 0 n)) (interpret (subseq str (+ n 14) (length str))))))))
    ((let ((n (find-substring str " is "))) ; check to see if two things are equal
       (if n
	   (return-from interpret (eql (interpret (subseq str 0 n)) (interpret (subseq str (+ n 4) (length str))))))))
    
    ;; arithmatic
    ((let ((n (position #\= str))) ; check for assignment
       (if n
	   (progn
	     (set-var (condense (subseq str 0 n)) (interpret (subseq str (+ n 1) (length str))))
	     t))))
    
    ; written integers become actual integers
    ((valid-int (condense str))
     (parse-integer (condense str)))
     
    ((let ((n (position #\+ str))) ; addition
       (if (and n (> n 0))
	   (+ (interpret (subseq str 0 n)) (interpret (subseq str (+ n 1) (length str)))))))
    ((let ((n (position #\- str))) ; subtraction
       (if (and n (> n 0))
	   (- (interpret (subseq str 0 n)) (interpret (subseq str (+ n 1) (length str)))))))
    ((let ((n (position #\* str))) ; multiplication
       (if (and n (> n 0))
	   (float (* (interpret (subseq str 0 n)) (interpret (subseq str (+ n 1) (length str))))))))
    ((let ((n (position #\/ str))) ; division
       (if (and n (> n 0))
	   (float (/ (interpret (subseq str 0 n)) (interpret (subseq str (+ n 1) (length str))))))))
    ((let ((n (position #\% str))) ; modulo
       (if (and n (> n 0))
	   (mod (interpret (subseq str 0 n)) (interpret (subseq str (+ n 1) (length str)))))))
    ((let ((n (position #\^ str))) ; modulo
       (if (and n (> n 0))
	   (expt (interpret (subseq str 0 n)) (interpret (subseq str (+ n 1) (length str)))))))
    

    ;; variables
    (t
     (let ((v (get-var (condense str) *vars*)))
	   (if v
	       v
	       (progn ;; error.
		 (format t "Error at '~a' (line ~d)" str (+ *currline* 1))
		 (terpri)
		 (exit)
		 nil))))))

; the first CLI argument is the file to interpret
(defvar *scriptfile* "")
(let ((cli-args (or 
   #+CLISP *args*
   #+SBCL *posix-argv*  
   #+LISPWORKS system:*line-arguments-list*
   #+CMU extensions:*command-line-words*
   nil)))
      (if (= (length cli-args) 2)
	  (setq *scriptfile* (nth 1 cli-args))))
(if (string= *scriptfile* "")
    (progn
      (format t "Please supply a script file as the first cli argument") (terpri)
      (exit)))

;; start interpreting the lines of the given file
(setq *vars* nil)
(setq *labels* nil)
(setq *varstack* nil)
(setq *returnstack* nil)
(setq *currline* 0)
(require 'uiop)
(let ((lines (uiop:read-file-lines *scriptfile*)))
  (loop
     (if (>= *currline* (length lines)) (return))
     (let ((line (condense-start (nth *currline* lines)))) ; condense-start to allow indentation
       
       ;(format t "line: '~a' (~d) ~a ~a" line *currline* *vars* *labels*) (terpri)
       
       (cond
	 ((string= line "") t)
	 ((char= (aref line 0) #\#) t) ; comment
	 ;; the end of some statement (like if)
	 ((string= line "end") t)
	 ;; functions
	 ((check-substring line "define block " 0)
	  (set-label (subseq line 13 (length line)) *currline*)
	  (let ((statement-levels-left 1))
	    (loop ; skip until the end of the if statement
	       (setq *currline* (+ *currline* 1))
	       (if (or
		    (check-substring (condense-start (nth *currline* lines)) "if " 0)
		    (check-substring (condense-start (nth *currline* lines)) "define block " 0))
		   (setq statement-levels-left (+ statement-levels-left 1))
		   (if (string= (condense-start (nth *currline* lines)) "end")
		       (setq statement-levels-left (- statement-levels-left 1))))
	       (if (<= statement-levels-left 0) (return)))))
	 ((check-substring line "jump to block " 0)
	  (push *currline* *returnstack*)
	  (setq *currline* (get-label (subseq line 14 (length line)) *labels*)))
	 ((string= line "return from block")
	  (setq *currline* (pop *returnstack*)))
	 ;; saving/reloading variables
	 ((check-substring line "save variable " 0)
	  (let ((name (subseq line 14 (length line))))
	    (push (get-var name *vars*) *varstack*)))
	 ((check-substring line "reload variable " 0)
	  (let ((v (pop *varstack*)))
	    (set-var (subseq line 16 (length line)) v)))
	 ;; labels and jumps
	 ((check-substring line "label " 0)
	  (set-label (subseq line 6 (length line)) *currline*))
	 ((check-substring line "jump to " 0)
	  (setq *currline* (get-label (subseq line 8 (length line)) *labels*)))
	 ;; decision making
	 ((check-substring line "if " 0) ; format: "if {logical expression}"
	  (if (null (interpret (subseq line 3 (length line)))) ; if it evaluates to nil, we should skip the if statement
	      (let ((statement-levels-left 1))
		(loop ; skip until the end of the if statement
		   (setq *currline* (+ *currline* 1))
		   (if (or
			(check-substring (condense-start (nth *currline* lines)) "if " 0)
			(check-substring (condense-start (nth *currline* lines)) "define block " 0))
			  (setq statement-levels-left (+ statement-levels-left 1))
			  (if (string= (condense-start (nth *currline* lines)) "end")
			      (setq statement-levels-left (- statement-levels-left 1))))
		      (if (<= statement-levels-left 0) (return))))))
	 ;; stopping the program
	 ((string= (condense-start line) "stop")
	  (exit))
	 
	 (t ; it's some other statement
	  (interpret line)))
       (setq *currline* (+ *currline* 1)))))

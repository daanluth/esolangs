# CrapLang
## introduction
Are you looking for something that is more clunky than assembly  
and also slower than python, all the while reinforcing bad programming practices?  
Look no further, because you have found it.  

CrapLang(tm) (PaTeNt PeNdIng) is interpreted by `parser.lisp` ,  
Which was written in Common Lisp.  
It is turing complete (in the most pointless kind of way)
and has all the aspects from other languages that everyone loves:  
- jumping to labels
- having to `end` each `if`
- no variable scoping (only global variables)
- no OOP
- no threading
- no lists
- no direct way to write real numbers (although you can use fractions)
- no parentheses, so everything has to be stored in temporary variables
- no `and`, `or` or `else`, only `if`
- confusing error messages
- It is easy to write (if by easy you mean tedious)
- probably some horrible implementation bugs
(a feature to keep you on your toes)

## Dependencies
the parser uses `uiop` which comes with `ASDF` .  
the parser was developed with sbcl (using emacs SLIME), so i am not sure  
if it will work with other lisp implementations.  

## Usage
With sbcl:
`$ sbcl --script parser.lisp <filename>`  
if you are using some other Common Lisp implementation  
just pass the file to be interpreted to the script as the first argument,  
and no other arguments.  

## Language Syntax
Everything is very verbose, for instance:  
`<=` becomes `is less than or equal to`  
`push {var}` becomes `save variable {var}`
#### Example:
```
recursions_left = 2
define block done
	if recursions_left is 0
		show "done"
		return from block
	end
	# push variables to the variable stack and recurse further
	save variable recursions_left
	recursions_left = recursions_left - 1
	show "recursing"
	jump to block done
	reload variable recursions_left
	return from block
end
# the user will be in a loop until they enter (2+3-5+3 =) 3
# after which they will jump into a block
quit_num = 2 +3 -5+ 3
write "quit_num: "
show quit_num
label loop1
	write "looping: "
	read into test_num
	if test_num is not quit_num
		write test_num
    		show " is not the quit number."
		jump to loop1
	end
jump to block done
show recursions_left
stop
```
#### Example Output:
quit_num: 3  
looping: 8  
8 is not the quit number.  
looping: 3  
recursing  
recursing  
done  
2  
#### Explanation:
`# {comment}` is a comment, inline comments are not supported.  
`define block {name}` creates a block, nesting these is allowed  
`write {expression}` means print without newline,  
`show {expression}`  means print with newline.  
`read into {varname}` (suprisingly) reads from stdin into the variable.  
`label {name}` creates a label at the line that it is on
`jump to {name}` jumps to the given label  
`jump to block {name}` jumps to the given block  
`stop` ends the program (not required at the end)
  
Variables do not need to be created before assignment,
and the rest is pretty self-explanatory.
Also, the indentation is not nessecary,
but it can be used as a (futile) attempt to make things more readable and clear.
  
## Quirks that i won't fix:
Labels and variable names can't contain "-",  
since "example-label" will be interpreted as "example" - "label",  
and the interpreter will crash  
  
The interpreter doesn't like characters after strings (even whitespace),  
so if you try "show 'a string'  " it will crash,  
if the other characters are meant to be there (like in 'if "a string" is 0') it's fine.
  
## For real though
I was learning common lisp and saw a video about parsing,  
so i felt inspired to make a programming language parser of my own.  
It's not great, but i still like it because i made it.  
The project took about 2 days to complete.  

## Legal stuff:
do whatever you want with this code, just don't sue me or claim you wrote it.  

# befungeInterpreter

a C based [Befunge](https://en.wikipedia.org/wiki/Befunge) interpreter (befunge.c)<br/>
and debugger (bdb.c).

## Installation:
$ sudo make install<br/>

## Usage:
$ befuge <file.bf><br/>
$ bdb <file.bf><br/>

#### bdb commands:
- n/next: step one instruction.<br/>
- c/continue: continue until we hit a breakpoint or the end.<br/>
- bp: set a breakpoint, x and y will be prompted.<br/>
- pc: show the current Program Contents.<br/>
- ps: show the current stack.<br/>
- v/verbose: toggle verbose mode,\n  when verbose mode is turned on every operation will be described.<br/>
- h/help: show a help menu.<br/>
- You can also hit enter to repeat the last command.<br/>

### SHA256 hashes:
bd04f6a67ffd3bc4aeba28abd419f20d327e51a44f0e68e7f3ca8d3654a69a41  bdb.c<br/>
a924dc3609c19191285d0b970c504ccd3f9fa0fb07e35b5b3e5bd72fd3ac4bf7  befunge.c<br/>

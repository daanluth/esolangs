#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdio.h>

/* ******************
 * This program is a gdb-like befuge debugger,
 * (it's essentially a befuge interpreter with some stuff added on)
 * you can pass it a befuge script as the first argument.
 * ****************** */

/*
The rules:
0-9 	Push this number on the stack
+ 	Addition: Pop a and b, then push a+b
- 	Subtraction: Pop a and b, then push b-a
* 	Multiplication: Pop a and b, then push a*b
/ 	Integer division: Pop a and b, then push b/a, rounded towards 0.
% 	Modulo: Pop a and b, then push the remainder of the integer division of b/a.
! 	Logical NOT: Pop a value. If the value is zero, push 1; otherwise, push zero.
` 	Greater than: Pop a and b, then push 1 if b>a, otherwise zero.
> 	Start moving right
< 	Start moving left
^ 	Start moving up
v 	Start moving down
? 	Start moving in a random cardinal direction
_ 	Pop a value; move right if value=0, left otherwise
| 	Pop a value; move down if value=0, up otherwise
" 	Start string mode: push each character's ASCII value all the way up to the next "
: 	Duplicate value on top of the stack
\ 	Swap two values on top of the stack
$ 	Pop value from the stack and discard it
. 	Pop value and output as an integer followed by a space
, 	Pop value and output as ASCII character
# 	Bridge: Skip next cell
p 	A "put" call (a way to store a value for later use). Pop y, x, and v, then change the character at (x,y) in the program to the character with ASCII value v
g 	A "get" call (a way to retrieve data in storage). Pop y and x, then push ASCII value of the character at that position in the program
& 	Ask user for a number and push it
~ 	Ask user for a character and push its ASCII value
@ 	End program
(space) 	No-op. Does nothing
*/

int isNum( char c ) {
	// just check every case
	return (c=='0' || c=='1' || c=='2' || c=='3' || c=='4' || c=='5' || c=='6' || c=='7' || c=='8' || c=='9');
}
int toNum( char c ) {
	// ASCII value to number (numbers start at 48)
	return ((int)c - 48);
}

// initialize a stack to push onto and pop from, and keep track of a stack pointer
long stack[10000] = {0};
long * stackPtr = stack;
// just for convenience
void push( long v ) {
	*stackPtr = v;
	stackPtr++;
}
long pop() {
	stackPtr--;
	return *stackPtr;
}

int main( int argc, char ** argv, char ** envp ) {
	if ( argc < 2 ) {
		printf("Please supply a befunge file as the first cli argument.\n");
		return 1;
	}
	
	// read the input file and store its contents in the contents char * buffer
	FILE *inFile;
	inFile = fopen(argv[1], "r");
	if ( inFile==NULL ) {
		printf("Could not open '%s'.", argv[1]);
		return 1;
	}
	fseek( inFile, 0, SEEK_END );
	long fileLength = ftell( inFile );
	char * content = malloc( fileLength );
	fseek( inFile, 0, SEEK_SET );
	if ( content != NULL ) {
		fread( content, 1, fileLength, inFile );
	}
	fclose( inFile );
	
	// keep a array with the length of every line in the file,
	// to make navigation easier.
	int nLines = 0;
	for (int i=0; i<fileLength; i++) {
		if (content[i]=='\n') {
			nLines++;
		}
	}
	int lineLengths[nLines];
	int charsBeforeLine[nLines];
	int currI = 0;
	int currLen = 0;
	for (int i=0; i<fileLength; i++) {
		currLen++;
		if (content[i]=='\n') {
			lineLengths[currI] = currLen;
			charsBeforeLine[currI] = i+1-currLen;
			currI++;
			currLen = 0;
		}
	}
	
	// keep track of the current direction, the current line and the current content[] index
	int dx = 1;
	int dy = 0;
	int y = 0;
	int i = 0;
	
	// seed the PRNG
	srand(time(NULL));
	// keep track of some states
	int skip = 0;
	int stringMode = 0;
	
	// and of the breakpoints
	int * breakpoints = malloc( fileLength );
	int nBreakpoints = 0;
	char lastCmd[512] = {0};
	int verboseMode = 0;
	int nextInstrBP = 1;
	
	// start interpreting
	while (1) {
		char curr = content[i];
		
		int bpHere = 0;
		for (int j=0; j<nBreakpoints; j++) {
			if ( breakpoints[j]==i ) {
				bpHere = 1;
				break;
			}
		}
		
		if ( bpHere==1 || nextInstrBP==1 ) {
			// display some stats
			printf("Current Instruction: '%c' at (%d, %d)\n", curr, i-charsBeforeLine[y], y);
			// display a menu
			while (1) {
				
				printf(">> ");
				// get the next instruction
				char cmd[512] = {0};
				fgets( cmd, 512, stdin );
				
				if ( strncmp( cmd, "\n", 1 )==0 ) {
					// repeat the last command
					strcpy( cmd, lastCmd );
				}
				// remember the last command
				strcpy( lastCmd, cmd );
				
				if ( strncmp( cmd, "n", 1 )==0 ) {
					// set a breakpoint at the next instruction
					nextInstrBP = 1;
					break;
				} else if ( strncmp( cmd, "c", 1 )==0 ) {
					nextInstrBP = 0;
					break;
				} else if ( strncmp( cmd, "pc", 2 )==0 ) {
					printf("Program Content:\n");
					int j = 0;
					printf("0: ");
					for (int i=0; i<fileLength; i++) {
						printf("%c", content[i]);
						if ( content[i]=='\n' && i!=fileLength-1 ) {
							j++;
							printf("%d: ", j);
						}
					}
					continue;
				} else if ( strncmp( cmd, "ps", 2 )==0 ) {
					printf("Current Stack:\n");
					long * tmpPtr = stack;
					while ( tmpPtr < stackPtr ) {
						printf("  %ld:\t%ld\t('%c')\n", (tmpPtr-stack), *tmpPtr, (int)*tmpPtr);
						tmpPtr++;
					}
					continue;
				} else if ( strncmp( cmd, "bp", 2 )==0 ) {
					// set a breakpoint at the specified instruction
					long tmpX, tmpY;
					char buffer[10] = {0};
					printf("breakpoint X: ");
					fgets( buffer, 10, stdin );
					tmpX = strtol(buffer, NULL, 10);
					printf("breakpoint Y: ");
					fgets( buffer, 10, stdin );
					tmpY = strtol(buffer, NULL, 10);
					
					breakpoints[nBreakpoints] = charsBeforeLine[tmpY] + tmpX;
					nBreakpoints++;
					printf("added breakpoint at (%ld, %ld)\n", tmpX, tmpY);
					continue;
				} else if ( strncmp( cmd, "v", 1 )==0 ) {
					// switch verbose mode
					verboseMode = 1 - verboseMode;
					if ( verboseMode==1 ) {
						printf("Verbose mode on.\n");
					} else {
						printf("Verbose mode off.\n");
					}
					continue;
				} else if ( strncmp( cmd, "h", 1 )==0 || strncmp( cmd, "?", 1 )==0 ) {
					printf("Debugger Commands:\n");
					printf(" n/next: step one instruction,\n");
					printf(" c/continue: continue until we hit a breakpoint or the end,\n");
					printf(" bp: set a breakpoint, x and y will be prompted,\n");
					printf(" pc: show the current Program Contents,\n");
					printf(" ps: show the current stack.\n");
					printf(" v/verbose: toggle verbose mode,\n  when verbose mode is turned on every operation will be described.\n");
					printf(" h/help: show a help menu.\n");
					printf("You can also hit enter to repeat the last command.\n");
					continue;
				}
			}
		}
		
		if ( verboseMode==1 && nextInstrBP==0 ) {
			// give some location information when skipping through a bunch of instructions
			printf("[(%d, %d)]: ", i-charsBeforeLine[y], y);
		}
		
		if (stringMode==1) {
			// if we are in string mode, push every ascii character up to the next " onto the stack
			if (curr=='"') {
				if ( verboseMode==1 ) { printf("Stopping string mode.\n"); }
				stringMode = 0;
			} else {
				if ( verboseMode==1 ) { printf("[Stringmode on] Pushing '%c' (%d)\n", curr, curr); }
				push( curr );
			}
		} else {
			if (!(skip==1)) {
				
				// just do what the rules specify
				if ( isNum(curr) ) {
					if ( verboseMode==1 ) { printf("Pushing '%c' to the stack\n", curr); }
					// 0-9 	Push this number on the stack
					push( toNum(curr) );
				} else if ( curr=='+' || curr=='-' || curr=='*' || curr=='/' || curr=='%' || curr=='`' || curr=='\\' ) {
					int a, b;
					a = pop();
					b = pop();
					if ( curr=='+' ) {
						if ( verboseMode==1 ) { printf("Doing %d + %d\n", a, b); }
						// + Addition: Pop a and b, then push a+b
						push( a+b );
					} else if ( curr=='-' ) {
						if ( verboseMode==1 ) { printf("Doing %d - %d\n", b, a); }
						// - Subtraction: Pop a and b, then push b-a
						push( b-a );
					} else if ( curr=='*' ) {
						if ( verboseMode==1 ) { printf("Doing %d * %d\n", a, b); }
						// * Multiplication: Pop a and b, then push a*b
						push( a*b );
					} else if ( curr=='/' ) {
						if ( verboseMode==1 ) { printf("Doing %d / %d\n", b, a); }
						// / Integer division: Pop a and b, then push b/a, rounded towards 0.
						push( (long)(b/a) );
					} else if ( curr=='%' ) {
						if ( verboseMode==1 ) { printf("Doing %d %% %d\n", b, a); }
						// % Modulo: Pop a and b, then push the remainder of the integer division of b/a.
						push( b%a );
					} else if ( curr=='`' ) {
						if ( verboseMode==1 ) { printf("Testing if %d > %d.\n", b, a); }
						// ` Greater than: Pop a and b, then push 1 if b>a, otherwise zero.
						if (b > a) {
							push( 1 );
						} else {
							push( 0 );
						}
					} else if ( curr=='\\' ) {
						if ( verboseMode==1 ) { printf("Swapping %d ('%c') and %d ('%c') on top of the stack.\n", a, a, b, b); }
						// \ Swap two values on top of the stack
						push( a );
						push( b );
					}
				} else if ( curr=='!' ) {
					int v = pop();
					if ( verboseMode==1 ) { printf("pushing logical NOT of value %d ('%c').\n", v, v); }
					// ! Logical NOT: Pop a value. If the value is zero, push 1; otherwise, push zero.
					if ( v == 0 ) {
						push( 1 );
					} else {
						push( 0 );
					}
				} else if ( curr=='.' ) {
					if ( verboseMode==1 ) { printf("Popping a value and displaying it as a number: '"); }
					// . Pop value and output as an integer followed by a space
					printf("%ld ", (long)pop());
					if ( verboseMode==1 ) { printf("'\n"); }
				} else if ( curr==',' ) {
					if ( verboseMode==1 ) { printf("Popping a value and displaying it as ASCII: '"); }
					// , Pop value and output as ASCII character
					printf("%c", (char)pop());
					if ( verboseMode==1 ) { printf("'\n"); }
				} else if ( curr=='v' ) {
					if ( verboseMode==1 ) { printf("moving down.\n"); }
					// start moving down
					dx = 0;
					dy = 1;
				} else if ( curr=='^' ) {
					if ( verboseMode==1 ) { printf("moving up.\n"); }
					// start moving up
					dx = 0;
					dy = -1;
				} else if ( curr=='>' ) {
					if ( verboseMode==1 ) { printf("moving right.\n"); }
					// start moving right
					dx = 1;
					dy = 0;
				} else if ( curr=='<' ) {
					if ( verboseMode==1 ) { printf("moving left.\n"); }
					// start moving left
					dx = -1;
					dy = 0;
				} else if ( curr=='?' ) {
					if ( verboseMode==1 ) { printf("Picking a random direction to move in: "); }
					// pick a random cardinal direction to move in
					int r = rand();
					if ( r < RAND_MAX*0.25 ) {
						if ( verboseMode==1 ) { printf("up\n"); }
						dx = 0;
						dy = -1;
					} else if ( r < RAND_MAX*0.5 ) {
						if ( verboseMode==1 ) { printf("down\n"); }
						dx = 0;
						dy = 1;
					} else if ( r < RAND_MAX*0.75 ) {
						if ( verboseMode==1 ) { printf("left\n"); }
						dx = -1;
						dy = 0;
					} else {
						if ( verboseMode==1 ) { printf("right\n"); }
						dx = 1;
						dy = 0;
					}
				} else if ( curr=='_' ) {
					// move right if the top value on the stack is 0, move left otherwise
					if ( pop() == 0 ) {
						if ( verboseMode==1 ) { printf("The popped value is 0, moving right.\n"); }
						dx = 1;
						dy = 0;
					} else {
						if ( verboseMode==1 ) { printf("The popped value is not 0, moving left.\n"); }
						dx = -1;
						dy = 0;
					}
				} else if ( curr=='|' ) {
					// move down if the top value on the stack is 0, move up otherwise
					if ( pop() == 0 ) {
						if ( verboseMode==1 ) { printf("The popped value is 0, moving down.\n"); }
						dx = 0;
						dy = 1;
					} else {
						if ( verboseMode==1 ) { printf("The popped value is not 0, moving up.\n"); }
						dx = 0;
						dy = -1;
					}
				} else if ( curr=='$' ) {
					long v = pop();
					// discard the top value on the stack
					if ( verboseMode==1 ) {
						printf("Discarding the topmost stack value (%ld, ('%c'))\n", v, (int)v);
					}
				} else if ( curr==':' ) {
					if ( verboseMode==1 ) {
						printf("Duplicating the topmost stack value (%ld, ('%c')).\n", *(stackPtr-1), (int)*(stackPtr-1));
					}
					// duplicate the topmost stack value
					push( *(stackPtr-1) );
				} else if ( curr=='#' ) {
					if ( verboseMode==1 ) {
						printf("Skipping the next instruction.\n");
					}
					// skip the next instruction
					skip = 1;
				} else if ( curr=='&' ) {
					// get a integer from the user and push it
					int c;
					while (1) {
						if ( verboseMode==1 ) { printf("Int: "); }
						c = (int) getchar();
						if ( isNum(c) ) {
							break;
						}
					}
					push( c-48 );
					// clear until the end of the line
					while (c != '\n' && c != '\r') {
						c = (int) getchar();
					}
				} else if ( curr=='"' ) {
					if ( verboseMode==1 ) {
						printf("Turning string mode on.\n");
					}
					// turn string mode on
					stringMode = 1;
				} else if ( curr=='~' ) {
					// get a character from the user and push it
					if ( verboseMode==1 ) { printf("Char: "); }
					int c = (int) getchar();
					push( c );
					// clear until the end of the line
					while (c != '\n' && c != '\r') {
						c = (int) getchar();
					}
				} else if ( curr=='p' ) {
					// "put" a value at a location
					int tmpX, tmpY;
					tmpY = pop();
					tmpX = pop();
					long v = pop();
					int j = 0;
					if ( verboseMode==1 ) {
						printf("Putting %ld ('%c') at (%d, %d)\n", v, (int)v, tmpX, tmpY);
					}
					while ( j < tmpY ) {
						tmpX += lineLengths[j];
						j++;
					}
					content[tmpX] = (char)v;
				} else if ( curr=='g' ) {
					// "get" a value from a location
					int tmpX, tmpY;
					tmpY = pop();
					tmpX = pop();
					int oldX;
					if ( verboseMode==1 ) { oldX = tmpX; }
					int j = 0;
					while ( j < tmpY ) {
						tmpX += lineLengths[j];
						j++;
					}
					if ( verboseMode==1 ) {
						printf("Getting %ld ('%c') from (%d, %d)\n", (long)content[tmpX], (int)content[tmpX], oldX, tmpY);
					}
					push( (long)content[tmpX] );
				} else if ( curr=='@' ) {
					if ( verboseMode==1 ) {
						printf("Stopping the program.\n");
					}
					// stop the program
					break;
				} else if ( curr==' ' ) {
					if ( verboseMode==1 ) { printf("NOP instruction.\n"); }
				}
				
			} else  {
				if ( verboseMode==1 ) { printf("Skipped a line.\n"); }
				skip = 0;
			}
		}
		
		// move the instruction pointer
		if ( dy == -1 ) {
			i -= lineLengths[y-1];
			y--;
		} else if ( dy == 1 ) {
			i += lineLengths[y];
			y++;
		}
		i += dx;
		// if we move over to another line, increase y
		if ( i-charsBeforeLine[y] >= lineLengths[y] ) {
			y++;
		}
		
		if ( i < 0 || y < 0 || y > nLines) {
			printf("The program 'crashed' into the edge of the program space at (%d, %d)\n", i-charsBeforeLine[y], y);
			break;
		}
		
	}
	
	// at the end, the stack pointer will be one unit above the stack
	stackPtr--;
	
	printf("\n\nProgram ended.\n");
	if (stackPtr<stack) {
		printf("The stack is empty.\n");
	} else {
		printf("The Stack: \n");
		while ( stackPtr >= stack ) {
			printf("%ld: %ld ('%c')\n", stackPtr-stack, *stackPtr, (int)*stackPtr);
			stackPtr--;
		}
	}
	
}

#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdio.h>

/* ******************
 * This program will interpret and execute befuge,
 * you can pass it a befuge script as the first argument.
 * ****************** */

/*
The rules:
0-9 	Push this number on the stack
+ 	Addition: Pop a and b, then push a+b
- 	Subtraction: Pop a and b, then push b-a
* 	Multiplication: Pop a and b, then push a*b
/ 	Integer division: Pop a and b, then push b/a, rounded towards 0.
% 	Modulo: Pop a and b, then push the remainder of the integer division of b/a.
! 	Logical NOT: Pop a value. If the value is zero, push 1; otherwise, push zero.
` 	Greater than: Pop a and b, then push 1 if b>a, otherwise zero.
> 	Start moving right
< 	Start moving left
^ 	Start moving up
v 	Start moving down
? 	Start moving in a random cardinal direction
_ 	Pop a value; move right if value=0, left otherwise
| 	Pop a value; move down if value=0, up otherwise
" 	Start string mode: push each character's ASCII value all the way up to the next "
: 	Duplicate value on top of the stack
\ 	Swap two values on top of the stack
$ 	Pop value from the stack and discard it
. 	Pop value and output as an integer followed by a space
, 	Pop value and output as ASCII character
# 	Bridge: Skip next cell
p 	A "put" call (a way to store a value for later use). Pop y, x, and v, then change the character at (x,y) in the program to the character with ASCII value v
g 	A "get" call (a way to retrieve data in storage). Pop y and x, then push ASCII value of the character at that position in the program
& 	Ask user for a number and push it
~ 	Ask user for a character and push its ASCII value
@ 	End program
(space) 	No-op. Does nothing
*/

int isNum( char c ) {
	// just check every case
	return (c=='0' || c=='1' || c=='2' || c=='3' || c=='4' || c=='5' || c=='6' || c=='7' || c=='8' || c=='9');
}
int toNum( char c ) {
	// ASCII value to number (numbers start at 48)
	return ((int)c - 48);
}

// initialize a large stack to push onto and pop from, and keep track of a stack pointer
long stack[10000] = {0};
long * stackPtr = stack;
// just for convenience
void push( long v ) {
	*stackPtr = v;
	stackPtr++;
}
long pop() {
	stackPtr--;
	return *stackPtr;
}

int main( int argc, char ** argv, char ** envp ) {
	
	if ( argc < 2 ) {
		printf("Please supply a befunge file as the first cli argument.\n");
		return 1;
	}
	
	// read the input file and store its contents in the contents char * buffer
	FILE *inFile;
	inFile = fopen(argv[1], "r");
	if ( inFile==NULL ) {
		printf("Could not open '%s'.", argv[1]);
		return 1;
	}
	fseek( inFile, 0, SEEK_END );
	long fileLength = ftell( inFile );
	char * content = malloc( fileLength );
	fseek( inFile, 0, SEEK_SET );
	if ( content != NULL ) {
		fread( content, 1, fileLength, inFile );
	}
	fclose( inFile );
	
	// keep a array with the length of every line in the file,
	// to make navigation easier.
	int nLines = 0;
	for (int i=0; i<fileLength; i++) {
		if (content[i]=='\n') {
			nLines++;
		}
	}
	int lineLengths[nLines];
	int charsBeforeLine[nLines];
	int currI = 0;
	int currLen = 0;
	for (int i=0; i<fileLength; i++) {
		currLen++;
		if (content[i]=='\t') {
			printf("WARNING: tab character found in document on line %d,\na tab will count as one character and may not work as expected,\nit is advised to use spaces instead.\n", currI+1);
		}
		if (content[i]=='\n') {
			lineLengths[currI] = currLen;
			charsBeforeLine[currI] = i+1-currLen;
			currI++;
			currLen = 0;
		}
	}
	
	// keep track of the current direction, the current line and the current content[] index
	int dx = 1;
	int dy = 0;
	int y = 0;
	int i = 0;
	
	// seed the PRNG
	srand(time(NULL));
	// keep track of some states
	int skip = 0;
	int stringMode = 0;
	
	// start interpreting
	while (1) {
		char curr = content[i];
		
		if (stringMode==1) {
			// if we are in string mode, push every ascii character up to the next " onto the stack
			if (curr=='"') {
				stringMode = 0;
			} else {
				push( curr );
			}
		} else {
			if (!(skip==1)) {
				
				// just do what the rules specify
				if ( isNum(curr) ) {
					// 0-9 	Push this number on the stack
					push( toNum(curr) );
				} else if ( curr=='+' || curr=='-' || curr=='*' || curr=='/' || curr=='%' || curr=='`' || curr=='\\' ) {
					int a, b;
					a = pop();
					b = pop();
					if ( curr=='+' ) {
						// + Addition: Pop a and b, then push a+b
						push( a+b );
					} else if ( curr=='-' ) {
						// - Subtraction: Pop a and b, then push b-a
						push( b-a );
					} else if ( curr=='*' ) {
						// * Multiplication: Pop a and b, then push a*b
						push( a*b );
					} else if ( curr=='/' ) {
						// / Integer division: Pop a and b, then push b/a, rounded towards 0.
						push( (long)(b/a) );
					} else if ( curr=='%' ) {
						// % Modulo: Pop a and b, then push the remainder of the integer division of b/a.
						push( b%a );
					} else if ( curr=='`' ) {
						// ` Greater than: Pop a and b, then push 1 if b>a, otherwise zero.
						if (b > a) {
							push( 1 );
						} else {
							push( 0 );
						}
					} else if ( curr=='\\' ) {
						// \ Swap two values on top of the stack
						push( a );
						push( b );
					}
				} else if ( curr=='!' ) {
					// ! Logical NOT: Pop a value. If the value is zero, push 1; otherwise, push zero.
					if ( pop() == 0 ) {
						push( 1 );
					} else {
						push( 0 );
					}
				} else if ( curr=='.' ) {
					// . Pop value and output as an integer followed by a space
					printf("%ld ", (long)pop());
				} else if ( curr==',' ) {
					// , Pop value and output as ASCII character
					printf("%c", (char)pop());
				} else if ( curr=='v' ) {
					// start moving down
					dx = 0;
					dy = 1;
				} else if ( curr=='^' ) {
					// start moving up
					dx = 0;
					dy = -1;
				} else if ( curr=='>' ) {
					// start moving right
					dx = 1;
					dy = 0;
				} else if ( curr=='<' ) {
					// start moving left
					dx = -1;
					dy = 0;
				} else if ( curr=='?' ) {
					// pick a random cardinal direction to move in
					int r = rand();
					if ( r < RAND_MAX*0.25 ) {
						dx = 0;
						dy = -1;
					} else if ( r < RAND_MAX*0.5 ) {
						dx = 0;
						dy = 1;
					} else if ( r < RAND_MAX*0.75 ) {
						dx = -1;
						dy = 0;
					} else {
						dx = 1;
						dy = 0;
					}
				} else if ( curr=='_' ) {
					// move right if the top value on the stack is 0, move left otherwise
					if ( pop() == 0 ) {
						dx = 1;
						dy = 0;
					} else {
						dx = -1;
						dy = 0;
					}
				} else if ( curr=='|' ) {
					// move down if the top value on the stack is 0, move up otherwise
					if ( pop() == 0 ) {
						dx = 0;
						dy = 1;
					} else {
						dx = 0;
						dy = -1;
					}
				} else if ( curr=='$' ) {
					// discard the top value on the stack
					pop();
				} else if ( curr==':' ) {
					// duplicate the topmost stack value
					push( *(stackPtr-1) );
				} else if ( curr=='#' ) {
					// skip the next instruction
					skip = 1;
				} else if ( curr=='&' ) {
					// get a integer from the user and push it
					int c;
					while (1) {
						//printf("Int: ");
						c = (int) getchar();
						if ( isNum(c) ) {
							break;
						}
					}
					push( c-48 );
					// clear until the end of the line
					while (c != '\n' && c != '\r') {
						c = (int) getchar();
					}
				} else if ( curr=='"' ) {
					// turn string mode on
					stringMode = 1;
				} else if ( curr=='~' ) {
					// get a character from the user and push it
					//printf("Char: ");
					int c = (int) getchar();
					push( c );
					// clear until the end of the line
					while (c != '\n' && c != '\r') {
						c = (int) getchar();
					}
				} else if ( curr=='p' ) {
					// "put" a value at a location
					int tmpX, tmpY, v;
					tmpY = pop();
					tmpX = pop();
					v = pop();
					int j = 0;
					while ( j < tmpY ) {
						tmpX += lineLengths[j];
						j++;
					}
					content[tmpX] = (char)v;
				} else if ( curr=='g' ) {
					// "get" a value from a location
					int tmpX, tmpY;
					tmpY = pop();
					tmpX = pop();
					int j = 0;
					while ( j < tmpY ) {
						tmpX += lineLengths[j];
						j++;
					}
					push( (long)content[tmpX] );
				} else if ( curr=='@' ) {
					// stop the program
					break;
				}
				
			} else  {
				skip = 0;
			}
		}
		
		// move the instruction pointer
		if ( dy == -1 ) {
			i -= lineLengths[y-1];
			y--;
		} else if ( dy == 1 ) {
			i += lineLengths[y];
			y++;
		}
		i += dx;
		// if we move over to another line, increase y
		if ( i-charsBeforeLine[y] >= lineLengths[y] ) {
			y++;
		}
	}
	
}

#!/usr/bin/python3
# -*- coding: utf-8 -*-

# helper function
def numOrString( st ):
	if st.replace(".","").isnumeric():
		return float(st)
	else:
		if (st[0]=='"') and (st[-1]=='"'):
			return st[1:-1]
		else:
			return st
def isNum( st ):
	return st.replace(".","").isnumeric()

from sys import argv
if len(argv) < 2:
	print("Please supply a usable file as the first CLI argument.")
	exit(1)
# get the content of the file
try:
	allChars = open(argv[1], "r").read()
except OSError as e:
	print("Error while opening and reading file: "+e)
	exit(1)

# to keep the instructions, variables and jump locations
instructions = [[] for _ in range(allChars.count("\n"))]
variables = {}
jumps = {}
# and the starting location
programStart = None

# parse the XML
currLine = 0
inTag = False
inString = False
currTag = "<" # just to start the process
charIndex = 0
while charIndex < len(allChars):
	
	c = allChars[charIndex]
	charIndex += 1
	
	# keep track of some stuff
	if c == '"':
		inString = not inString
	elif c == '\n':
		currLine += 1
	
	# keep track of the current XML tag
	if inTag:
		currTag += c
	
	# manage comments
	if (currTag.endswith("<!--")):
		# skip to the end of the comment
		tmp = ""
		while ( not tmp=="-->" ):
			tmp += allChars[charIndex]
			if len(tmp) > 3:
				tmp = tmp[1:]
			charIndex += 1
		# for the next tag
		currTag = "<"
		inTag = False
	
	if (c == '<') and not (inTag or inString):
		# start of a XML tag
		inTag = True
	elif (c == '>'):
		
		# is this actually the end of a tag?
		if not inString:
			
			# is this tag interesting?
			if (currTag[1] != '/'):
				
				# determine the tag type and the parameters
				# ( "<meta charset='utf-8'>" ==> "meta" & "charset='utf-8'" )
				
				# get the tag type
				tagType = ""
				i = 1
				while not (currTag[i]==' ' or currTag[i]=='>'):
					tagType += currTag[i]
					i += 1
				
				# now parse the parameters
				parameters = {}
				if i < len(currTag)-1:
					
					newKey = ""
					newValue = ""
					currPart = 0 # 0=key, 1=value
					inStr = False
					while i < len(currTag):
						
						if currPart == 0:
							# key parsing
							if currTag[i]=='=':
								currPart = 1
							elif not currTag[i]==' ':
								newKey += currTag[i]
						else:
							# value parsing
							if (not inStr) and (currTag[i]==' ' or currTag[i]=='>'):
								# save this key/value pair
								parameters[ newKey ] = numOrString( newValue )
								# more pairs might be ahead
								newKey = ""
								newValue = ""
								currPart = 0
							else:
								newValue += currTag[i]
						
						# keep a local-ish version of the inString flag
						if currTag[i]=='"':
							inStr = not inStr
						
						# continue to the next character
						i += 1
				
				# is this the program start point
				if ( programStart==None ) and ( tagType=="body" ):
					programStart = currLine
				
				# interpret this combination of tag and parameters
				if ( tagType=='p' and "value" in parameters ):
					instructions[ currLine ].append( f"PRNT{parameters['value']}" )
				elif ( tagType=="fieldset" and "name" in parameters and "value" in parameters ):
					instructions[ currLine ].append( f"VSET{parameters['name']}/{parameters['value']}" )
				elif ( tagType=="var" and "name" in parameters and "value" in parameters ):
					variables[parameters["name"]] = parameters["value"]
				elif ( tagType=="i" and "name" in parameters ):
					instructions[ currLine ].append( f"INCR{parameters['name']}" )
				elif ( tagType=="q" and "type" in parameters and "in1" in parameters and "in2" in parameters and "out" in parameters ):
					opType = parameters["type"].upper()
					if opType in ["ADD","SUB","MUL","DIV","MOD"]:
						instructions[ currLine ].append( f"A{opType}{parameters['in1']}/{parameters['in2']}/{parameters['out']}" )
				elif ( tagType=='a' and "href" in parameters ):
					if ("test" in parameters):
						instructions[ currLine ].append( f"CJMP{parameters['href']}/{parameters['test']}" )
					else:
						instructions[ currLine ].append( f"JUMP{parameters['href']}" )
				elif ( tagType=="div" and "name" in parameters ):
					jumps[ parameters["name"] ] = currLine
				elif ( tagType=="input" and "type" in parameters and "name" in parameters ):
					inType = parameters["type"].upper()
					if inType in ["NUM","STR"]:
						instructions[ currLine ].append( f"GET{inType[0]}{parameters['name']}" )
				elif ( tagType=="time" and "wait" in parameters ):
					instructions[ currLine ].append( f"WAIT{parameters['wait']}" )
			# reset for the next tag
			currTag = "<"
			inTag = False

if (programStart == None):
	print("ERROR: no program start location found (<body> tag missing).")
	exit(1)

from time import sleep

# now that we are done interpreting start running the generated instructions
instructionIndex = programStart
while 0 <= instructionIndex < len(instructions):
	
	# interpret and execute all instructions on this line
	for instr in instructions[instructionIndex]:
		opCode = instr[:4]
		content = instr[4:]
		if (opCode == "PRNT"):
			# interpret the printable line
			printStr = ""
			# special chars
			content = content.replace("\\n","\n")
			# replace variables with their values
			varName = ""
			inVar = False
			for i, c in enumerate(content):
				
				if c=='$':
					varName += c
					inVar = True
				elif c in " \t\n" and inVar:
					# cut off the first "$"
					varName = varName[1:]
					# maybe replace the value with the actual variable value?
					if (varName[0]=='$'):
						# no, it has been escaped
						printStr += varName
					else:
						printStr += str( variables[varName] )+c
					# reset for any next variables
					varName = ""
					inVar = False
				else:
					if inVar:
						varName += c
					else:
						printStr += c
				
			# now print the result
			print( printStr, end="" )
		elif (opCode == "JUMP"):
			# unconditional jump
			instructionIndex = jumps[ content ]
		elif (opCode == "CJMP"):
			# conditional jump
			jmpIndex, jmpCondition = content.split("/")
			jmpIndex = jumps[ jmpIndex ]
			if ( "==" in jmpCondition ):
				in1, in2 = jmpCondition.split("==")
			elif ( "!=" in jmpCondition ):
				in1, in2 = jmpCondition.split("!=")
			elif ( "<=" in jmpCondition ):
				in1, in2 = jmpCondition.split("<=")
			elif ( ">=" in jmpCondition ):
				in1, in2 = jmpCondition.split(">=")
			elif ( "<" in jmpCondition ):
				in1, in2 = jmpCondition.split("<")
			elif ( ">" in jmpCondition ):
				in1, in2 = jmpCondition.split(">")
			
			# variables or numbers?
			if isNum( in1 ):
				in1 = float(in1)
			else:
				in1 = variables[ in1 ]
			if isNum( in2 ):
				in2 = float(in2)
			else:
				in2 = variables[ in2 ]
			
			if ( "==" in jmpCondition ):
				if ( in1 == in2 ):
					instructionIndex = jmpIndex
			elif ( "!=" in jmpCondition ):
				if ( in1 != in2 ):
					instructionIndex = jmpIndex
			elif ( "<=" in jmpCondition ):
				if ( in1 <= in2 ):
					instructionIndex = jmpIndex
			elif ( ">=" in jmpCondition ):
				if ( in1 >= in2 ):
					instructionIndex = jmpIndex
			elif ( "<" in jmpCondition ):
				if ( in1 < in2 ):
					instructionIndex = jmpIndex
			elif ( ">" in jmpCondition ):
				if ( in1 > in2 ):
					instructionIndex = jmpIndex
		elif (opCode == "VSET"):
			# set the value of $varName to $newVal,
			# or the value of the variable named $newVal
			varName, newVal = content.split("/")
			if isNum(newVal):
				newVal = float(newVal)
			else:
				newVal = variables[ newVal ]
			variables[ varName ] = newVal
		elif (opCode == "INCR"):
			# increment a value
			variables[ content ] += 1
		elif (opCode == "GETS"):
			# get a string and store it in the specified variable
			variables[ content ] = input()
		elif (opCode == "GETN"):
			# get a number and store it in the specified variable
			while True:
				ret = input()
				if isNum( ret ):
					break
			variables[ content ] = float(ret)
		elif (opCode[1:] in ["ADD","SUB","MUL","DIV","MOD"]):
			opCode = opCode[1:]
			# get the variables involved
			in1, in2, out = content.split("/")
			if isNum(in1):
				in1 = float(in1)
			else:
				in1 = variables[ in1 ]
			if isNum(in2):
				in2 = float(in2)
			else:
				in2 = variables[ in2 ]
			# do the arithmatic
			if (opCode=="ADD"):
				variables[ out ] = in1 + in2
			elif (opCode=="SUB"):
				variables[ out ] = in1 - in2
			elif (opCode=="MUL"):
				variables[ out ] = in1 * in2
			elif (opCode=="DIV"):
				variables[ out ] = in1 / in2
			elif (opCode=="MOD"):
				variables[ out ] = in1 % in2
		elif (opCode == "WAIT"):
			sleep( float(content) )
	# move over to the next contentuction
	instructionIndex += 1

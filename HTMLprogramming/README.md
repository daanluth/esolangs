# HTML programming

### Description
A parser that interprets HTML tags as program instructions,<br/>
It's what everyone has been waiting for,<br/>
the rules and tags you can use are in RULES.txt<br/>
<br/>
### Usage
$ python3 parser.py <file.html><br/>
<br/>
You will, need to have python3 installed,<br/>
The script was made and tested with python3.6.9<br/>
but it should work with python3.*<br/>
### SHA256 hash:
712dc8a2139d4c9b58442111d284feb1728b5cd8b827611c34b5fcf115815d17  parser.py<br/>
